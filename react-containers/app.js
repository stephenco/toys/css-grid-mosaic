import React, {Component} from 'react';
import ReactDOM from 'react-dom';

class GridElement extends Component {
  constructor() {
    super();
  }

  render() {
    const color = this.props.color;
    const style = {
      backgroundColor: 'rgba(' + color.r + ',' + color.g + ',' + color.b + ', 1)'
    };
    return (
      <div className="grid-element" style={style}></div>
    );
  }
}

class Grid extends Component {
  constructor() {
    super();
  }

  count(to) {
      var result = [];
      for (var i = 0; i < to; i++) {
        result.push(+i);
      }
      return result;
  };

  ids() {
    return this.count(this.props.height * this.props.width);
  }

  styles() {
    return (<style> {"\
        body { \
          background-color: #456; \
          font-family: sans-serif; \
          color: #abc; \
          padding: 0; \
          margin: 0; \
        } \
        .grid { \
          display: grid; \
          grid-template-columns: repeat(" + this.props.width + ", 1fr); \
          align-items: stretch; \
          width: 100%; \
          height: 100vh; \
        } \
        .grid-element { \
          display: block; \
          overflow: hidden; \
          text-align: center; \
          border: 1px solid #678; \
        } \
      "}</style>);
  }

  randomIn(min, max) {
    return Math.round(min + Math.random() * (max - min));
  }

  color() {
    return {
      r: this.randomIn(180, 200),
      g: this.randomIn(180, 200),
      b: this.randomIn(180, 250)
    };
  }

  elements() {
    return this.ids().map(id => (
      <GridElement key={id} color={this.color()} />
    ));
  }

  render() {
    return (
      <div className="grid">
        {this.styles()}
        {this.elements()}
      </div>
    );
  }
}

class App extends Component {
    render () {
        return (
          <div className="app">
            <Grid width="40" height="30" />
          </div>
        );
    }
}

ReactDOM.render(<App />, document.getElementById('app'));
